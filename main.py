from src.pick_and_pack import PickAndPack
from sciroc_ep5_logging.sciroc_ep5_logger import LogStatusSciRoc

if __name__ == "__main__":
    task  = PickAndPack(LogStatusSciRoc("LASR"))
    task.start()
    task.end()