#!/usr/bin/env python

class PickAndPack():

    def __init__(self, logger):
        self.logger = logger

    def start(self):
        # Log the start of the run
        self.logger.log_run_started()
        self.pick("chickpeas")
        self.place("chickpeas")
        self.end()

    def pick(self, object_name):
        # ... pick up the object
        self.logger.log_object_picked(object_name)

    def place(self, object_name):
        # ... place the object
        self.logger.log_object_placed(object_name)

    def end(self):
        self.logger.log_run_stopped()